package edu.mcckc.tests;

import edu.mcckc.domain.PasswordManager;
import org.junit.Assert;
import org.junit.Test;

/**
 * Sandy Hull
 * CSIS 222
 * FINAL
 * Due Monday May 15th @9:30pm
 */

public class TestPasswordManager
{

    @Test
    public void TestPasswordManagerConstructor()
    {
        PasswordManager pw = new PasswordManager();
        Assert.assertNotNull(pw);
    }

    @Test
    public void TestPasswordAllUpper()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeAllUpperCase(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[A-Z]*$"));
    }

    @Test
    public void TestPasswordAllUpperEZ()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeAllUpperCase(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[A-NP-Z]*$"));
    }

    @Test
    public void TestPasswordAllLowerCase()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeAllLowerCase(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[a-z]*$"));
    }

    @Test
    public void TestPasswordAllLowerCaseEZ()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeAllLowerCase(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[a-km-z]*$"));
    }

    @Test
    public void TestPasswordAllDigits()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[0-9]*$"));
    }

    @Test
    public void TestPasswordAllDigitsEZ()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[2-9]*$"));
    }

    @Test
    public void TestPasswordAllSpecial()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeSpecialChars(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|]*$"));
    }

    @Test
    public void TestPasswordAllSpecialAndDigits()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|0-9]*$"));
    }

    @Test
    public void TestPasswordAllSpecialAndDigitsAndUpper()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeAllUpperCase(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|0-9A-Z]*$"));
    }

    @Test
    public void TestPasswordAllSpecialAndDigitsAndLower()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeAllLowerCase(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|0-9a-z]*$"));
    }

    @Test
    public void TestPasswordAllSpecialAndDigitsAndMixed()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeMixedCase(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|0-9a-zA-Z]*$"));
    }

    @Test
    public void TestPasswordAllSpecialAndDigitsAndMixedEZ()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeMixedCase(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|2-9a-km-zA-NP-Z]*$"));
    }

    @Test
    public void TestPasswordLength()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeMixedCase(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(8);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().length() >= 8 && pw.getNewPassword().length() <= 12);
    }

    @Test
    public void TestPasswordLengthSame()
    {

        PasswordManager pw = new PasswordManager();
        pw.setSpecialCharacters("+-=_@#$^&;:,.<>~[](){}?!|");
        pw.setbIncludeDigits(true);
        pw.setbIncludeSpecialChars(true);
        pw.setbIncludeMixedCase(true);
        pw.setbIncludeEZToRead(true);
        pw.setMaxLength(12);
        pw.setMinLength(12);
        pw.generateNewPassword();

        Assert.assertTrue(pw.getNewPassword().length() == 12);
    }
}
