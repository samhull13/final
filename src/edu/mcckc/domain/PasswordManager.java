package edu.mcckc.domain;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Sandy Hull
 * CSIS 222
 * FINAL
 * Due Monday May 15th @9:30pm
 */

public class PasswordManager
{
    private Random generator;
    private String upperAlphabet;
    private String lowerAlphabet;
    private String digits;
    private String specialCharacters;
    private String ezUpperLetter;
    private String ezLowerLetter;
    private String ezNumbers;
    private String newPassword;
    private String shuffleString;
    private boolean bIncludeDigits;
    private boolean bIncludeSpecialChars;
    private boolean bIncludeEZToRead;
    private boolean bIncludeAllUpperCase;
    private boolean bIncludeAllLowerCase;
    private boolean bIncludeMixedCase;
    private int minLength;
    private int maxLength;
    private int realLength;
    private int difference;
    private ArrayList<String> optionArray;
    private ArrayList<String> shuffleMe;


    public PasswordManager()
    {
        generator = new Random(System.currentTimeMillis());
        upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        digits = "0123456789";
        ezUpperLetter = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        ezLowerLetter = "abcdefghijkmnopqrstuvwxyz";
        ezNumbers = "23456789";
    }

    public String getNewPassword()
    {
        return newPassword;
    }

    public void generateNewPassword()
    {
        //initialize string builder object
        StringBuilder sb = new StringBuilder();

        //get difference between max and min length required
        difference = maxLength - minLength;

        //get random length of pw from between the max and min chosen
        if (difference == 0)
        {
            realLength = minLength;
            Logger.getLogger(this.getClass()).debug(String.format("NEW PASSWORD LENGTH IS: %d", realLength));
        }
        else
        {
            realLength = generator.nextInt(difference +1);
            realLength = minLength + realLength;
            Logger.getLogger(this.getClass()).debug(String.format("NEW PASSWORD LENGTH IS: %d", realLength));
        }

        //check options and add to an option array so we include each option at least one time
        optionArray = new ArrayList<>();

        //check for all lower case and EZ
        if (bIncludeAllLowerCase)
        {
            if (!bIncludeEZToRead)
            {
                optionArray.add("1");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - EZ TO READ LOWER CASE");
            }
            else
            {
                optionArray.add("2");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - ALL LOWER CASE");
            }
        }

        //check for all upper case and EZ
        if (bIncludeAllUpperCase)
        {
            if (!bIncludeEZToRead)
            {
                optionArray.add("3");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - EZ TO READ UPPER CASE");
            }
            else
            {
                optionArray.add("4");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - ALL UPPER CASE");
            }
        }

        //check for mixed case and EZ
        if (bIncludeMixedCase)
        {
            if (!bIncludeEZToRead)
            {
                optionArray.add("1");
                optionArray.add("3");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - EZ TO READ MIXED CASE");
            }
            else
            {
                optionArray.add("2");
                optionArray.add("4");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - MIXED CASE");
            }
        }

        //check for include digits and EZ
        if (bIncludeDigits)
        {
            if (!bIncludeEZToRead)
            {
                optionArray.add("5");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - EZ TO READ DIGITS");
            }
            else
            {
                optionArray.add("6");
                Logger.getLogger(this.getClass()).debug("OPTION ADDED - ALL DIGITS");
            }
        }

        //check for special chars
        if (bIncludeSpecialChars)
        {
            optionArray.add("7");
            Logger.getLogger(this.getClass()).debug("OPTION ADDED - SPECIAL CHARACTERS");
        }

        //pad the options array to full length of pw so our pw isn't too short using random options from those selected
        int currentArraySize = optionArray.size();
        int leftovers = realLength - currentArraySize;
        Logger.getLogger(this.getClass()).debug(String.format("NEED TO ADD %d SPACES FOR PADDING", leftovers));
        for (int j=0; j < leftovers; j++)
        {
            int getPad = generator.nextInt(currentArraySize);
            String padding = optionArray.get(getPad);
            optionArray.add(padding);
        }

        //get the pw characters by matching number in options array with random char from each type selected
        for (String temp : optionArray)
        {
            if (temp.equals("1"))
            {
                int i = generator.nextInt(lowerAlphabet.length());
                sb.append(lowerAlphabet.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", lowerAlphabet.substring(i, i + 1)));
            }
            if (temp.equals("2"))
            {
                int i = generator.nextInt(ezLowerLetter.length());
                sb.append(ezLowerLetter.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", ezLowerLetter.substring(i, i + 1)));
            }
            if (temp.equals("3"))
            {
                int i = generator.nextInt(upperAlphabet.length());
                sb.append(upperAlphabet.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", upperAlphabet.substring(i, i + 1)));
            }
            if (temp.equals("4"))
            {
                int i = generator.nextInt(ezUpperLetter.length());
                sb.append(ezUpperLetter.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", ezUpperLetter.substring(i, i + 1)));
            }
            if (temp.equals("5"))
            {
                int i = generator.nextInt(digits.length());
                sb.append(digits.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", digits.substring(i, i + 1)));
            }
            if (temp.equals("6"))
            {
                int i = generator.nextInt(ezNumbers.length());
                sb.append(ezNumbers.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", ezNumbers.substring(i, i + 1)));
            }
            if (temp.equals("7"))
            {
                int i = generator.nextInt(specialCharacters.length());
                sb.append(specialCharacters.substring(i, i + 1));
                Logger.getLogger(this.getClass()).debug(String.format("ADDED %s TO PASSWORD", specialCharacters.substring(i, i + 1)));
            }
        }

        //put characters for pw into an array list
        shuffleString = sb.toString();
        shuffleMe = new ArrayList<>();
        for (int k = 0; k < shuffleString.length(); k++)
        {
            shuffleMe.add(shuffleString.substring(k, k + 1));
        }

        //shuffle the list
        Collections.shuffle(shuffleMe);
        StringBuilder afterShuffle = new StringBuilder();
        for (String temp : shuffleMe)
        {
            afterShuffle.append(temp);
        }

        //set newPassword to the newly shuffled pw
        setNewPassword(afterShuffle.toString());
        Logger.getLogger(this.getClass()).debug(String.format("NEW PASSWORD %s", newPassword));
    }


    public void setSpecialCharacters(String specialCharacters)
    {
        this.specialCharacters = specialCharacters;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    public void setbIncludeDigits(boolean bIncludeDigits)
    {
        this.bIncludeDigits = true;
    }

    public void setbIncludeSpecialChars(boolean bIncludeSpecialChars)
    {
        this.bIncludeSpecialChars = true;
    }

    public void setbIncludeEZToRead(boolean bIncludeEZToRead)
    {
        this.bIncludeEZToRead = true;
    }

    public void setbIncludeAllUpperCase(boolean bIncludeAllUpperCase)
    {
        this.bIncludeAllUpperCase = true;
    }

    public void setbIncludeAllLowerCase(boolean bIncludeAllLowerCase)
    {
        this.bIncludeAllLowerCase = true;
    }

    public void setbIncludeMixedCase(boolean bIncludeMixedCase)
    {
        this.bIncludeMixedCase = true;
    }

    public void setMinLength(int minLength)
    {
        this.minLength = minLength;
    }

    public void setMaxLength(int maxLength)
    {
        this.maxLength = maxLength;
    }

    public boolean getbIncludeDigits() {
        return bIncludeDigits;
    }
}
