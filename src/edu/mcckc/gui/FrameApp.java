package edu.mcckc.gui;

import javax.swing.*;

/**
 * Sandy Hull
 * CSIS 222
 * FINAL
 * Due Monday May 15th @9:30pm
 */

public class FrameApp extends JFrame
{
    private PanelApp pnlApp;

    public FrameApp()
    {
        pnlApp = new PanelApp();
        add(pnlApp);
    }
}
