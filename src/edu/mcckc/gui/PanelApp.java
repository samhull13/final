package edu.mcckc.gui;

import edu.mcckc.domain.PasswordManager;
import edu.mcckc.exceptions.PasswordException;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Sandy Hull
 * CSIS 222
 * FINAL
 * Due Monday May 15th @9:30pm
 */

public class PanelApp extends JPanel implements ActionListener
{
    private PasswordManager generator;

    private JPanel row1;
    private JPanel row2;
    private JPanel row3;
    private JPanel row4;
    private JPanel row5;
    private JPanel row6;
    private JPanel row7;

    private JLabel lblSpecialChars;
    private JTextField txtSpecialChars;
    private JLabel lblNewPassword;
    private JTextField txtNewPassword;

    private JCheckBox chkIncludeDigits;
    private JCheckBox chkIncludeSpecialChars;
    private JCheckBox chkIncludeEZToRead;

    private JRadioButton radAllUpperCase;
    private JRadioButton radAllLowerCase;
    private JRadioButton radMixedCase;
    private ButtonGroup radButtonGroup;

    private JLabel lblMinLength;
    private JTextField txtMinLength;
    private JLabel lblMaxLength;
    private JTextField txtMaxLength;


    private JButton btnGenerate;
    private String specialCharacters = "+-=_@#$^&;:,.<>~[](){}?!|";
    private String excptText;

    private boolean badPassword;


    public PanelApp()
    {
        lblSpecialChars = new JLabel("Special Characters: ");
        txtSpecialChars = new JTextField(25);
        txtSpecialChars.setText(specialCharacters);

        chkIncludeDigits = new JCheckBox("Incl. Digits");
        chkIncludeSpecialChars = new JCheckBox("Incl. Special Chars");
        chkIncludeEZToRead = new JCheckBox("Only EZ to Read Chars");


        radAllUpperCase = new JRadioButton("All Upper Case");
        radAllLowerCase = new JRadioButton("All Lower Case");
        radMixedCase = new JRadioButton("Mixed Case");

        //need to do this to make the radio buttons one at a time
        radButtonGroup = new ButtonGroup();
        radButtonGroup.add(radAllUpperCase);
        radButtonGroup.add(radAllLowerCase);
        radButtonGroup.add(radMixedCase);

        lblMinLength = new JLabel("Min Length: ");
        txtMinLength = new JTextField(5);

        lblMaxLength = new JLabel("Max Length: ");
        txtMaxLength = new JTextField(5);

        btnGenerate = new JButton("Generate Password");

        lblNewPassword = new JLabel("New Password: ");
        txtNewPassword = new JTextField(40);


        row1 = new JPanel();
        row1.add(lblSpecialChars);
        row1.add(txtSpecialChars);

        row2 = new JPanel();
        row2.add(chkIncludeDigits);
        row2.add(chkIncludeSpecialChars);
        row2.add(chkIncludeEZToRead);

        row3 = new JPanel();
        row3.add(radAllUpperCase);
        row3.add(radAllLowerCase);
        row3.add(radMixedCase);

        row4 = new JPanel();
        row4.add(lblMinLength);
        row4.add(txtMinLength);

        row5 = new JPanel();
        row5.add(lblMaxLength);
        row5.add(txtMaxLength);

        row6 = new JPanel();
        row6.add(btnGenerate);

        row7 = new JPanel();
        row7.add(lblNewPassword);
        row7.add(txtNewPassword);

        setLayout(new GridLayout(7, 1));

        add(row1);
        add(row2);
        add(row3);
        add(row4);
        add(row5);
        add(row6);
        add(row7);

        btnGenerate.addActionListener(this);
        btnGenerate.setActionCommand("generate");
    }

    //validate all entries before passing to PasswordManager
    private void validateEntries() throws PasswordException
    {
        try
        {

            //if Incl. Special Chars is selected make sure there are some in the field
            if (chkIncludeSpecialChars.isSelected() && txtSpecialChars.getText().equals(""))
            {
                Logger.getLogger(this.getClass()).error("No special characters are entered.");
                excptText = "Please enter at least one Special Character";
                badPassword = true;
                return;
            }
            else
            {
                if (chkIncludeSpecialChars.isSelected())
                {
                    //validate the special characters against list given
                    String tempSpecial = txtSpecialChars.getText();
                    boolean testedCharacters = tempSpecial.matches("[+\\-=_@#$^&;:,.<>~\\[\\](){}?!|]*$");
                    if (testedCharacters == true)
                    {
                        //set special characters to what is in the field
                        generator.setSpecialCharacters(tempSpecial);
                        Logger.getLogger(this.getClass()).debug(String.format("SPECIAL CHARACTERS WE WILL USE: %s", tempSpecial));
                    }
                    else
                    {
                        Logger.getLogger(this.getClass()).error("Incorrect special characters are entered");
                        excptText = "Use only one or more of these characters: +-=_@#$^&;:,.<>~[](){}?!|";
                        badPassword = true;
                        return;
                    }

                    generator.setbIncludeSpecialChars(true);
                }
            }

            //validate the min and max entered
            //check for numbers only
            if ((!txtMinLength.getText().matches("[0-9]+")) || (!txtMaxLength.getText().matches("[0-9]+")))
            {
                Logger.getLogger(this.getClass()).error("Entry not an int");
                excptText = "Please enter valid data";
                badPassword = true;
                return;
            }

            //get entered values into integer variables
            int intMinLenght = Integer.valueOf(txtMinLength.getText());
            int intMaxLength = Integer.valueOf(txtMaxLength.getText());
            //check for minimum being at least 8 chars
            if (intMinLenght >= 8)
            {
                //check for min being less than max
                if (intMinLenght > intMaxLength)
                {
                    Logger.getLogger(this.getClass()).error("Maximum greater than minimum");
                    excptText = new PasswordException("MIN PW length must be less than the MAX PW length ").toString();
                    badPassword = true;
                    return;
                }
                else
                {
                    //set min and max in PasswordManager
                    generator.setMinLength(intMinLenght);
                    generator.setMaxLength(intMaxLength);
                }
            }
            else
            {
                Logger.getLogger(this.getClass()).error("Minimum too short");
                excptText = new PasswordException("Minimum password length must be 8 characters").toString();
                badPassword = true;
                return;
            }

            //check if at least one radio button is selected
            if (!radAllLowerCase.isSelected() && !radAllUpperCase.isSelected() && !radMixedCase.isSelected())
            {
                Logger.getLogger(this.getClass()).error("No button was checked.");
                excptText = new PasswordException("Please select a Radio Button").toString();
                badPassword = true;
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(this.getClass()).error("Error", new PasswordException("Cannot validate entries"));
        }
    }


    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getActionCommand().equalsIgnoreCase("generate"))
        {
            Logger.getLogger(this.getClass()).debug("NEW PASSWORD GENERATION STARTED");
            //instantiate new PasswordManager object
            generator = new PasswordManager();

            //validate all entered values before passing to Password Manager
            try
            {
               validateEntries();
            }
            catch (Exception ex)
            {
               Logger.getLogger(this.getClass()).error("Validating the entered data did not work");
            }

            Logger.getLogger(this.getClass()).debug("ALL ENTRIES VALIDATED");

            //check to see if an entry was bad
            if (badPassword)
            {
                txtNewPassword.setText(excptText);
                badPassword = false;
                Logger.getLogger(this.getClass()).debug("BAD PASSWORD ENTRY - GENERATOR ENDED");
            }
            else
            {
                //if all entries were valid set options in PasswordManager
                if (chkIncludeEZToRead.isSelected())
                {
                    generator.setbIncludeEZToRead(true);
                }
                if (radAllLowerCase.isSelected())
                {
                    generator.setbIncludeAllLowerCase(true);
                }
                if (radAllUpperCase.isSelected())
                {
                    generator.setbIncludeAllUpperCase(true);
                }
                if (radMixedCase.isSelected())
                {
                    generator.setbIncludeMixedCase(true);
                }
                if (chkIncludeDigits.isSelected())
                {
                    generator.setbIncludeDigits(true);
                }

                //call PasswordManager to create new password
                generator.generateNewPassword();

                //set new password to text field
                txtNewPassword.setText(generator.getNewPassword());
                Logger.getLogger(this.getClass()).debug("GENERATOR ENDED");
            }

        }
    }
}
