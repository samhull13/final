package edu.mcckc.exceptions;

/**
 * Sandy Hull
 * CSIS 222
 * FINAL
 * Due Monday May 15th @9:30pm
 */

public class PasswordException extends Exception
{
    public PasswordException()
    {
        super("An unknown Password Exception occurred.");
    }

    public PasswordException(String message)
    {
        super(message);
    }
}
